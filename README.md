# Precommit Lint

To test:

1. Clone this repo.
2. Cd into it.
3. Make some changes to some JavaScript files, maybe add some files.
4. Commit.

Notice how your "broken" syntax styles got fixed without you doing anything.

Notice how unrelated (not staged) files were untouched. (You can see this
already in action by checking `angular/src/app/app.module.ts`, it has double
quotes because that's the latest change in the `.prettierrc.json`, but the rest
of the files don't have double quotes, they have single quotes, because they
haven't yet been included in a commit.)

**Prettier will only be run on files that are staged in Git.**
